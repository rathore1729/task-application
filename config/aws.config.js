/**
 * @author Rajendra
 */

const AWS = require('aws-sdk');
const config = require('./env.config').aws;

AWS.config.update({
    secretAccessKey : config.secretAccessKey,
    accessKeyId : config.accessKeyId,
    region : config.region
});

//Initiate S3 client using AWS Instance
const S3 = new AWS.S3();

//Export AWS services indeed
module.exports = {
    S3:S3
}


/**
 * @author Rajendra
 */

const logger = require('./logger.config');
const env = require('./env.config');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');

//Read configuration properties from environment
const dbConfig = env.database;
const mongoOpts = { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true };

//Check if the selected environment is test then inject in-memory database
if (env.profile === 'test') {
    
    mongoose.Promise = Promise;

    logger.info('Creating Mongo Memory Server');
    const mongoServer = new MongoMemoryServer();
    
    mongoServer.getConnectionString().then((mongoUri) => {

        mongoose.connect(mongoUri, mongoOpts);

        mongoose.connection.on('error', (e) => {
            if (e.message.code === 'ETIMEDOUT') {
                logger.error(e);
                mongoose.connect(mongoUri, mongooseOpts);
            }
            logger.error(e);
        });

        mongoose.connection.once('open', () => {
            logger.info(`Database successfully connected to ${mongoUri}`);
        });
    });
} else {
    var uri = '';
    if(dbConfig.srvRecord){
        uri = `mongodb+srv://${dbConfig.username}:${dbConfig.password}@${dbConfig.host}/${dbConfig.name}`;
    }else{
        uri = `mongodb://${dbConfig.host}:${dbConfig.port}/${dbConfig.name}`;
        mongoOpts.user = dbConfig.username;
        mongoOpts.pass = dbConfig.password;
    }

    mongoose.connect(uri, mongoOpts).then(() => logger.info(`Database succesfully connected to ${uri}`))
        .catch((err) => {
            logger.error('Could not connect to the database. Exiting now...', err);
            process.exit();
        });

    //Handle after initial connection was established
    mongoose.connection.on('error', err => {
        logger.error(err);
    });

    //Handle disconnection issues
    mongoose.connection.on('disconnected', () => {
        logger.error('Database disconnected.');
    });

    //set other properties here
    mongoose.set('debug', dbConfig.debug);
}

module.exports = mongoose;
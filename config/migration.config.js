/**
 * @author Rajendra
 * Using npm package https://github.com/seppevs/migrate-mongo
 */
const env = require(`./env.config`);
const appRoot = require('app-root-path');
const logger = require('./logger.config');
const migrator = require('migrate-mongo');
const datasource = require('./database.config');

const dbConfig = env.database;

migrator.config.shouldExist = () => true;
migrator.config.read = async function() {
    var url = dbConfig.srvRecord?`mongodb+srv://${datasource.connection.user}:${datasource.connection.pass}@${dbConfig.host}`:`mongodb://${datasource.connection.host}:${datasource.connection.port}`;
    return {
        mongodb: {
            url: url,
            databaseName: datasource.connection.name,
            options: {
                useNewUrlParser: true, // removes a deprecation warning when connecting
                //   connectTimeoutMS: 3600000, // increase connection timeout to 1 hour
                //   socketTimeoutMS: 3600000, // increase socket timeout to 1 hour
                useUnifiedTopology: true
            }
        },
    
        // The migrations dir, can be an relative or absolute path. Only edit this when really necessary.
        migrationsDir: `${appRoot}/migrations`,
    
        // The mongodb collection where the applied changes are stored. Only edit this when really necessary.
        changelogCollectionName: "migrations",
    }
};

migrator.start = async function () { 
    logger.info('Starting migrations');
    const connection = await migrator.database.connect();
    logger.info('Migrating on database: '+connection.db.databaseName);
    const migrated = await migrator.up(connection.db);
    migrated.forEach(fileName => logger.info('Migrated : '+ fileName));
    logger.info('Migrations completed.');
};

module.exports=migrator;
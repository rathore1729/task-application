/**
 * @author Rajendra
 */
const env = require('./env.config');
const logger = require('./logger.config');
const datasource = require('./database.config');
const migrator = require('./migration.config');
const swagger = require('./swagger.config');
const aws = require('./aws.config');

//Export configured services
module.exports = {
    //Runtime environment properties configured with application
    env,
    //logger to handle logging in files
    logger,
    //Data source with connection
    datasource,
    //Migrator for data migrations
    migrator,
    //Can be used to initialize authentication in app 
    // authenticator,
    //Swagger config object to handle API documentation
    swagger,
    //AWS services
    aws
};
{
  "swagger": "2.0",
  "info": {
    "version": "v1",
    "title": "Sample Application",
    "description": "Sample Application API's"
  },
  "basePath": "/",
  "tags": [
    {
      "name": "User Session",
      "description": "API's for user session"
    },
    {
      "name": "Miscellaneous",
      "description": "Miscellaneous API's to support application features like autocomplete, download image etc"
    }
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "securityDefinitions": {
    "OAuth2Password": {
      "type": "oauth2",
      "description": "Authorize a user",
      "flow": "password",
      "tokenUrl": "/oauth/token",
      "refreshUrl": "/oauth/token"
    }
  },
  "paths": {
    "/userinfo": {
      "description": "API to get the logged in user profile",
      "get": {
        "summary": "API to get logged in user details",
        "description": "Returs logged in user profile",
        "tags": [
          "User Session"
        ],
        "security": [
          {
            "OAuth2Password": []
          }
        ],
        "responses": {
          "200": {
            "description": "Logged in User profile",
            "schema": {
              "type": "object",
              "$ref": "#/definitions/User"
            }
          },
          "401": {
            "description": "Authorization information is missing or invalid."
          },
          "5XX": {
            "description": "Unexpected error",
            "schema": {
              "type": "object",
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/logout": {
      "description": "API to logout",
      "post": {
        "summary": "API to logout through the application",
        "tags": [
          "User Session"
        ],
        "security": [
          {
            "OAuth2Password": []
          }
        ],
        "responses": {
          "200": {
            "description": "User logged out successfully",
            "schema": {
              "type": "object",
              "properties": {
                "msg": {
                  "type": "string"
                }
              }
            }
          },
          "400": {
            "description": "Invalid request"
          }
        }
      }
    },
    "/autocomplete": {
      "description": "API to get specified field values from a collection/model",
      "get": {
        "summary": "API to get specified field values from a collection/model",
        "tags": [
          "Miscellaneous"
        ],
        "security": [
          {
            "OAuth2Password": []
          }
        ],
        "parameters": [
          {
            "type": "string",
            "name": "Accept",
            "in": "header",
            "description": "API version in format application/vnd.sample-v1.0.0+json",
            "required": true
          },
          {
            "in": "query",
            "name": "field",
            "type": "string",
            "description": "Field key from a model or collection needs to autocomplete",
            "required": true
          },
          {
            "in": "query",
            "name": "collection",
            "type": "string",
            "description": "Model or collection where field exists",
            "required": true
          },
          {
            "in": "query",
            "name": "filters",
            "type": "array",
            "description": "array of key values to filter list based on collection fields, format to use filters[key]=value",
            "required": false
          }
        ],
        "responses": {
          "200": {
            "description": "List of field values",
            "schema": {
              "type": "object",
              "properties": {
                "pagination": {
                  "type": "object",
                  "$ref": "#/definitions/Pagination"
                },
                "results": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              }
            }
          },
          "400": {
            "description": "field/collection is required",
            "schema": {
              "type": "object",
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/images/{id}": {
      "description": "Images related API's",
      "get": {
        "summary": "API to download an image using image id",
        "tags": [
          "Miscellaneous"
        ],
        "security": [
          {
            "OAuth2Password": []
          }
        ],
        "parameters": [
          {
            "type": "string",
            "name": "Accept",
            "in": "header",
            "description": "API version in format application/vnd.sample-v1.0.0+json",
            "required": true
          },
          {
            "in": "param",
            "name": "id",
            "type": "string",
            "description": "Image id",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Image file",
            "schema": {
              "type": "object"
            }
          },
          "400": {
            "description": "Image does not exists.",
            "schema": {
              "type": "object",
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Pagination": {
      "type": "object",
      "properties": {
        "offset": {
          "type": "integer"
        },
        "limit": {
          "type": "integer"
        },
        "total": {
          "type": "integer"
        }
      }
    },
    "Error": {
      "type": "object",
      "properties": {
        "status": {
          "type": "integer"
        },
        "code": {
          "type": "string"
        },
        "field": {
          "type": "string"
        },
        "message": {
          "type": "string"
        }
      }
    },
    "User": {
      "type": "object",
      "properties": {
        "_id": {
          "type": "string",
          "description": "Unique identity of the user"
        },
        "username": {
          "type": "string"
        },
        "firstName": {
          "type": "string"
        },
        "lastName": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "avatar": {
          "type": "string"
        }
      }
    }
  }
}
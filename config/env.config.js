/**
 * @author Rajendra
 * 
 */
require('dotenv').config();
const convict = require('convict');
const appRoot = require('app-root-path');
const envPrefix = "";//Environment variable name prifix e.g. PROJECT_

const config = convict({
	profile: {
		format: ["dev", "qa", "staging", "prod", "test"],
		default: "dev",
		arg: "node-env",
		env: "NODE_ENV"
	},
	timezone:{
		default: "America/New_York",
		arg: "timezone",
		env: "TZ"
	},
	server: {
		url: {
			host: {
				doc: "The application server host.",
				default: "localhost",
				arg: "host",
				env: `${envPrefix}HOST`
			},
			port: {
				format: "port",
				doc: "The application server port.",
				default: 3000,
				arg: "port",
				env: `${envPrefix}PORT`
			},
			protocol: {
				format: ["http", "https"],
				doc: "The application server port.",
				default: "http",
				arg: "protocol",
				env: `${envPrefix}URL_PROTOCOL`
			}
		},
		timeout: {
			doc: "The server response timeout.",
			default: "1s",
			arg: "timeout",
			env: `${envPrefix}TIMEOUT`
		}
	},
	logging: {
		file: {
			level: {
				doc: "The output log level in log file",
				format: ["error", "warn", "info", "debug"],
				default: "info",
				arg: "file-log-level",
				env: `${envPrefix}LOG_LEVEL`
			},
			filename: {
				doc: "The log file name including path",
				default: "sample-%DATE%.log",
				format: String,
				arg: "log-file"
			},
			dirname: {
				doc: "The directory name to save log files to",
				default: `${appRoot}/logs`,
				format: String,
				arg: "log-dir",
				env: `${envPrefix}LOG_DIR`
			},
			prepend: {
				format: Boolean,
				default: true
			},
			maxsize: {
				format: Number,
				default: 5242880 //5MB
			},
			maxFiles: {
				format: Number,
				default: 10
			},
			handleExceptions: {
				doc: "Handle file logging exception",
				format: Boolean,
				default: true
			},
			json: {
				format: Boolean,
				default: false
			},
			colorize: {
				format: Boolean,
				default: false
			},
			datePattern: {
				format: String,
				default: "YYYY-MM-DD"
			},
			zippedArchive: {
				format: Boolean,
				default: true
			}
		},
		console: {
			json: {
				format: Boolean,
				default: false
			},
			handleExceptions: {
				doc: "Handle console logging exception",
				format: Boolean,
				default: false
			},
			colorize: {
				format: Boolean,
				default: true
			}
		}
	},
	database: {
		name: {
			doc: "The name of the database.",
			format: String,
			default: "sample",
			arg: "db-name",
			env: `${envPrefix}DB_NAME`
		},
		host: {
			doc: "The host or Ip where the database hosted.",
			format: String,
			default: "localhost",
			arg: "db-host",
			env: `${envPrefix}DB_HOST`
		},
		port: {
			doc: "The port number on which database hosted.",
			format: "port",
			default: 27017,
			arg: "db-port",
			env: `${envPrefix}DB_PORT`
		},
		username: {
			doc: "database user name",
			format: String,
			default: null,
			arg: "db-user",
			env: `${envPrefix}DB_USER`
		},
		password: {
			doc: "database password",
			format: String,
			default: null,
			sensitive: true,
			arg: "db-password",
			env: `${envPrefix}DB_PASSWORD`
		},
		srvRecord:{
			format: Boolean,
			default: false,
			arg: "db-srv-record",
			env: `${envPrefix}DB_SRV_RECORD`
		},
		debug: {
			format: Boolean,
			default: false,
			arg: "db-debug"
		},
		options: {
			format: Object,
			default: {}
		}
	},
	swagger:{
		explorer:{
			format: Boolean,
			default: true
		}
	},
	aws: {
		secretAccessKey : {
			format: String,
			doc : "Secret Access Key",
			default: null,
			arg : "aws-access-key",
			env: `${envPrefix}AWS_ACCESS_KEY`
		},
		accessKeyId : {
			format: String,
			doc : "Access Key Id",
			default: null,
			arg : "aws-access-id",
			env: `${envPrefix}AWS_ACCESS_ID`
		},
		region : {
			format: String,
			doc : "Region",
			default : 'us-east-1',
			arg : "aws-region",
			env: `${envPrefix}AWS_REGION`
		},
		s3:{
			bucketName : {
				format: String,
				doc : "Bucket Name",
				default: null,
				arg : "s3-bucket-name",
				env: `${envPrefix}S3_BUCKET_NAME`
			}
		}
	},
	schedulers: {
		// alertScheduler: {
		// 	enabled: {
		// 		format: Boolean,
		// 		default: true
		// 	},
		// 	cron: {
		// 		format: String,
		// 		doc: "Cron to schedule",
		// 		default: "0 6 * * *"
		// 	}
		// }
	}
});

const env = config.get('profile');
config.loadFile(`${appRoot}/env/${env}.json`); // load relevant environment file
config.validate(); // throws error if config does not conform to schema

module.exports = config.getProperties();
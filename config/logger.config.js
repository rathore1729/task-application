/**
 * @author Rajendra
 */

var winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const env = require(`./env.config`);

// define the custom settings for each transport (file, console)
var options = env.logging;

// instantiate a new Winston Logger with the settings defined above
var logger =  winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.printf(info => `${info.timestamp} ${info.level} :: ${info.message}`)
    ),
    transports: [
        new winston.transports.Console(options.console),
        new DailyRotateFile(options.file)
    ],
    exitOnError: false, // do not exit on handled exceptions
});

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
    write: function (message, encoding) {
        // use the 'info' log level so the output will be picked up by both transports (file and console)
        logger.info(message);
    },
};

module.exports = logger;
/**
 * Swagger Configuration for API's documentation
 * @link https://www.npmjs.com/package/swagger-ui-express
 * @author Rajendra
 */
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const env = require('./env.config');

module.exports = {
    serve: swaggerUi.serve,
    setup: swaggerUi.setup(swaggerDocument, env.swagger)
};
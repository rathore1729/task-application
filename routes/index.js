/**
 * @author Rajendra
 */
const express = require("express");
const router = express.Router();
const taskRoute = require('../packages/task/task.routes');
const miscellaneousRoute = require('../packages/misc/miscellaneous.routes');
 
router.use('/tasks', taskRoute);
router.use(miscellaneousRoute);

module.exports = router;
/**
 * @author Rajendra
 */

const appRoot = require('app-root-path');
const taskService = require('./task.service');
const { logger } = require(`${appRoot}/config`);
const { ResponseHandler } = require(`${appRoot}/utils`);


/**
 * @param {Request} req 
 * @param {Response} res 
 */
exports.addToDoTask = (req, res, next) => {
    let task = req.body.task;
    logger.debug(`Request received to add a new task`);
    taskService.addToDoTask(task, function (err, data) {
        ResponseHandler.handleResponse(err, data, req, res, next);
    });
};

/**
 * @param {Request} req 
 * @param {Response} res 
 */
exports.updateToDoTask = (req, res, next) => {
    let id = req.params.id;
    let task = req.body.task;
    logger.debug(`Request received to update task with id ${id}`);
    taskService.updateToDoTask(id, task, function (err, data) {
        ResponseHandler.handleResponse(err, data, req, res, next);
    });
};

/**
 * @param {Request} req 
 * @param {Response} res 
 */
exports.deleteToDoTask = (req, res, next) => {
    let id = req.params.id;
    logger.debug(`Request received to delete task with id ${id}`);
    taskService.deleteToDoTask(id, function (err, data) {
        ResponseHandler.handleResponse(err, data, req, res, next);
    });
};

/**
 * @param {Request} req 
 * @param {Response} res 
 */
exports.getSubTask = (req, res, next) => {
    let id = req.params.id;
    logger.debug(`Request received to get subtasks for task with id ${id}`);
    taskService.getSubTask(id, function (err, data) {
        ResponseHandler.handleResponse(err, data, req, res, next);
    });
};
var mongoose = require('mongoose');

var TaskSchema = new mongoose.Schema({
    task_id: {
        type: String,
        unique: true
    },
    title: {
        type: String,
        required: true
    },
    timestamp: {
        type: Date
    },
    status: {
        type: String,
        enum: ['ACTIVE','COMPLETE']
    },
    parent_id: {
        type: String
    }
});

module.exports = mongoose.model('tasks', TaskSchema);
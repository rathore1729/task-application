/**
 * @author Rajendra
 */

const express = require('express');
const router = express.Router();
const taskController = require('./task.controller');
const AuthInterceptor = require('../../utils/auth-interceptor');

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
	next();
});

router.post('/add', taskController.addToDoTask)
router.post('/update/:id', AuthInterceptor.authorizeRequest, taskController.updateToDoTask)
router.delete('/delete/:id', AuthInterceptor.authorizeRequest, taskController.deleteToDoTask)
router.get('/subtasks/:id', taskController.getSubTask)

module.exports = router;
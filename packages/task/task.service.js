/**
 * @author Rajendra
 */
const appRoot = require('app-root-path');
const { logger } = require(`${appRoot}/config`);
const errors = require('common-errors');
const Task = require('./task.model');

exports.addToDoTask = (task, callback) => {
    var newTask = new Task(task);
    newTask.save().then(task => {
        if (task) {
            console.log(task)
            return callback(null, task);
        } else {
            return callback(new errors.NotSupportedError('Something went wrong!'));
        }
    }).catch(err => {
        logger.error(err);
    });
}

exports.updateToDoTask = (id, task, callback) => {
    Task.replaceOne({task_id: id}, task).then(task => {
        if (task) {
            console.log(task)
            return callback(null, {success: true});
        } else {
            return callback(new errors.NotSupportedError('Something went wrong!'));
        }
    }).catch(err => {
        logger.error(err);
    });
}

exports.deleteToDoTask = (id, callback) => {
    Task.remove({task_id: id}).then(task => {
        if (task) {
            console.log(task)
            return callback(null, task);
        } else {
            return callback(new errors.NotSupportedError('Something went wrong!'));
        }
    }).catch(err => {
        logger.error(err);
    });
}

exports.getSubTask = (id, callback) => {
    Task.find({parent_id: id}).then(tasks => {
        if (tasks && tasks.length > 0) {
            return callback(null, tasks);
        } else {
            return callback(null, []);
        }
    }).catch(err => {
        logger.error(err);
    });
}
/**
 * @author Rajendra
 */

const appRoot = require('app-root-path');
const miscellaneousService = require('./miscellaneous.service');
const { logger } = require(`${appRoot}/config`);
const { ResponseHandler } = require(`${appRoot}/utils`);

/**
 * Controller to support feature auto-complete a field value
 * @param {Request} req 
 * @param {Response} res 
 */
exports.autocomplete = (req, res, next) => {

    var field = req.query.field;
    var collection = req.query.collection;
    var filters = req.query.filters;

    logger.debug(`Request received to get ${field} values from ${collection}`);
    miscellaneousService.autocomplete(field, collection, filters, function (err, data) {
        var resultSet = null;
        if(!err){
            resultSet = {
                results:[],
                pagination: {
                    total: 0
                }
            };
            if(data){
                resultSet.results= data;
                resultSet.pagination.total = data.length;
            }
        }       
        ResponseHandler.handleResponse(err, resultSet, req, res, next);
    });
};

/**
 * Controller to get image from id
 * @param {Request} req
 * @param {Response} res
 */
exports.getImage = (req, res, next) => {
    var imageId = req.params.id;
    logger.debug(`Request received to get the image with id ${imageId}`);
    miscellaneousService.getImage(imageId, function(err, image) {
        if(err){
            ResponseHandler.handleResponse(err, null, req, res, next);
        }else{
            res.status(200);
            res.write(image.Body, 'binary');
            res.end(); 
        }
    });
};
/**
 * @author Rajendra
 */

const express = require('express');
const router = express.Router();
const miscellaneousController = require('./miscellaneous.controller');
const versionRouter = require('express-version-route');

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    next();
});

//router version map
const autocompleteRoutesMap = new Map();
autocompleteRoutesMap.set('1.0', miscellaneousController.autocomplete);

// route to get a field values from a collection to support autocomplete feature on UI
router.get('/autocomplete',
    versionRouter.route(autocompleteRoutesMap));


//router version map
const imageRoutesMap = new Map();
imageRoutesMap.set('1.0', miscellaneousController.getImage);

//route to download images
router.get('/images/:id',
    versionRouter.route(imageRoutesMap)
);

module.exports = router;
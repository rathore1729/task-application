/**
 * @author Rajendra
 */
const appRoot = require('app-root-path');
const { logger, aws, env } = require(`${appRoot}/config`);
const { materialService, partService } = require(`${appRoot}/services`);
const errors = require('common-errors');
const SUPPORTED_COLLECTIONS = ["material"]; 

/**
 * Method to get field values from a collection based on filters
 * @param {*} callback 
 */
exports.autocomplete = (field, collection, filters, callback) => {
    logger.debug(`Get ${field} values from ${collection} collection based on filters`);
    if(!field){
        return callback(new errors.ValidationError('field is required', 'InvalidRequest', 'field'), null);
    }
    if(!collection){
        return callback(new errors.ValidationError('collection is required', 'InvalidRequest', 'collection'), null);
    }
    
    switch(collection){
        case 'material':
            materialService.getFieldValues(field, filters, function(err, data){
                return callback(err, data);
            });
        break;
        case 'part':
            partService.getFieldValues(field, filters, function(err, data){
                return callback(err, data);
            });
        break;
        default:
            return callback(new errors.ValidationError('Supported collections are '+SUPPORTED_COLLECTIONS.toString(), 'InvalidRequest', 'collection'), null);
        break;
    }
};

/**
 * Method to get image from s3 by imageId
 * @param {*} imageId
 * @param {*} callback
 */
exports.getImage = (imageId, callback) => {
    logger.debug(`Preparing to get image with id ${imageId} from s3`);
    var opts = {
        Bucket : env.aws.s3.bucketName,
        Key : imageId
    }

    aws.S3.getObject(opts, function(err, data){
        if(err){
            logger.error(err);
            return callback(err, null);
        }
        if(data){
            return callback(null, data);
        } else{
            return callback(new errors.NotFoundError('Image does not exists.'), null);
        }
    });
}
/**
 * As per API Guidelines
 * Endpoint Return Values
 *   GET: 200: Object with pagination element, and results array, 
 *   POST: 201: Singular object which is created
 *   PUT: 200: Singular object which was updated
 *   PATCH: 200: Singular object which was updated
 *   DELETE: 204: No body
 * 
 * @param {*} err 
 * @param {*} data 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
module.exports.handleResponse = function (err, data, req, res, next) {
    if (data) {
        let status;
        switch (req.method) {
            case 'GET':
            case 'PUT':
            case 'PATCH':
                status = 200;
                break;
            case 'POST':
                status = 201;
                break;
            case 'DELETE':
                status = 204;
                break;
            default:
                status = res.status | 200;
                break;
        }
        res.setHeader('Content-Type', 'application/json');
        res.status(status).send(data);
    } else {
        return next(err);
    }
}

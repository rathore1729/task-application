/**
 * @author Rajendra
 */
const appRoot = require('app-root-path');
const { logger } = require(`${appRoot}/config`);
const error = require('common-errors');

/**
 * Error handler method to handle defined or undefined errors
 * @param {Error} err 
 * @param {Request} req
 * @param {Response} res
 * @param {Next} next 
 */
module.exports.handleError = function (err, req, res, next) {
    if (!err) {
        if (next) {
            return next();
        }
        return res.end();
    }

    let status = err.statusCode || err.status || 500;
    let message = err.message || 'Something unexpected happened';
    let field = err.field || null;
    let code = err.code || null;

    if (err instanceof error.ValidationError || err instanceof error.ArgumentError) {
        status = 400;
    } else if (err instanceof error.AuthenticationRequiredError) {
        status = 401;
    } else if (err instanceof error.NotPermittedError) {
        status = 403;
    } else if (err instanceof error.ArgumentNullError || err instanceof error.NotFoundError) {
        status = 404;
    } else if (err instanceof error.NotSupportedError) {
        status = 405;
    } else if (err instanceof error.AlreadyInUseError) {
        status = 409;
    } else if (err.name === 'ServiceUnavailableError'){
        status = 504;
    }

    //TODO : send notifications to development team 
    //log error before sending as response
    //logger.error(err);

    if (!res.headersSent) {
        res.status(status).send({
            message: message,
            code: code,
            field: field,
            status: status
        });
    }
};
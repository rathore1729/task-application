/**
 * @author Rajendra
 */

// Interceptor to check if the authorization header is equal to admin, send error with 401 otherwise
module.exports.authorizeRequest = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        if (authHeader.toString().toLowerCase() !== 'admin') {
            res.sendStatus(401);
        } else {
            next();
        }
    } else {
        res.sendStatus(401);
    }
};
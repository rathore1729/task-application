/**
 * @author Rajendra
 */

//import requrired libs
const app = require('./app');
const { env , logger } = require('./config');
const http = require(env.server.url.protocol);

//read host and port from configured properties
var url = env.server.url;

//create server
http.createServer(app).listen(url.port, url.host, () => {
	logger.info("Server ready at " + url.protocol + "://" + url.host + ":" + url.port);
	logger.info(`Active Profile [${env.profile}]`);
	logger.info(`Timezone [${env.timezone}]`);
});
/**
 * @author Rajendra
 */

module.exports = {
	up(db) {
		return db.collection("oauth_client_details").insertOne({
			clientId: 'sample_web_client',
			clientSecret: 'sample_web_client_secret',
			grants: ['password', 'client_credentials', 'refresh_token'],
			redirectUris: [],
			accessTokenValidity: 86400,
			refreshTokenValidity: 31104000
		});
	}
};

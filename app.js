/**
 * @author Rajendra
 */

const express = require('express');
const bodyParser = require('body-parser');
const versionRequest = require('express-version-request');
const boolParser = require('express-query-boolean');
const morgan = require('morgan');
const cors = require('cors');
const { logger, migrator, datasource, swagger } = require('./config');
const routes = require('./routes');
const { ErrorHandler } = require('./utils');
const app = express();

//CORS configuration
app.use(cors());

//Use body parser to parse request body in json
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(boolParser());

//set middleware to parse Accept Header for API version
app.use(versionRequest.setVersionByAcceptHeader());

//Setup logger pluging into app
app.use(morgan('combined', { stream: logger.stream }));

//Integrate API documentation plugin using swagger
app.use('/api-docs', swagger.serve, swagger.setup);

//Integrate all application routes
app.use("/", routes);

//initialize authenticator for authentication handling
// app.use(authenticator.initialize());

//Integrate error handler
app.use(ErrorHandler.handleError);

//Start migrations once datasourse connected
datasource.connection.once('open', function () {
    migrator.start();
});

module.exports = app;
